package com.silvanacorrea.librosapp.model

import java.io.Serializable

data class Libros(val nombre: String, val autor: String, val tipo: String, val img: Int): Serializable{

}