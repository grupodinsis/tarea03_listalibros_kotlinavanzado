package com.silvanacorrea.librosapp.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.silvanacorrea.librosapp.R
import com.silvanacorrea.librosapp.adapter.LibroAdapter
import com.silvanacorrea.librosapp.databinding.ActivityMainBinding
import com.silvanacorrea.librosapp.model.Libros

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private  var libros: MutableList<Libros> = mutableListOf<Libros>()

    private lateinit var adaptador : LibroAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        setupAdapter()
        loadData()
    }

    private fun setupAdapter(){
        adaptador = LibroAdapter(){

            val bundle = Bundle().apply {
                putSerializable("key_libro",it)
            }

            val intent = Intent(this,DetalleLibroActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)

        }
        binding.rvLibros.adapter = adaptador
        binding.rvLibros.layoutManager = LinearLayoutManager(this)
    }

    private fun loadData(){

        libros.add(Libros("La vida es sueño","Pedro Calderón de la Barca","Teatro barroco", R.mipmap.la_vida))
        libros.add(Libros("Crimen y Castigo", "Fiodor Dostoiesvski","Narrativa", R.mipmap.crimenycastigo))
        libros.add(Libros("El retrato de Dorian Gray", "Oscar Wilde", "Narrativa", R.mipmap.doriangray))

        adaptador.updateList(libros)

    }
}
