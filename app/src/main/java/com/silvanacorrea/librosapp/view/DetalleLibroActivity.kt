package com.silvanacorrea.librosapp.view

import android.os.Binder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.silvanacorrea.librosapp.R
import com.silvanacorrea.librosapp.databinding.ActivityDetalleLibroBinding
import com.silvanacorrea.librosapp.model.Libros

class DetalleLibroActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetalleLibroBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_detalle_libro)

        binding = ActivityDetalleLibroBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        var bundle = intent.extras
        bundle?.let {
            val libro = it.getSerializable("key_libro") as Libros

            binding.tvNombre.text = libro.nombre
            binding.tvAutor.text = libro.autor
            binding.tvTipo.text = libro.tipo
            binding.imgView.setImageResource(libro.img)

        }
    }
}