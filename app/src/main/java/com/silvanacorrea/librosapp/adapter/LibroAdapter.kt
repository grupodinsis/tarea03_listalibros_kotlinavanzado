package com.silvanacorrea.librosapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.silvanacorrea.librosapp.R
import com.silvanacorrea.librosapp.databinding.ItemLibroBinding
import com.silvanacorrea.librosapp.model.Libros

class LibroAdapter(private var libros: MutableList<Libros> = mutableListOf(),
                    val itemCallLibro: (Libros) -> Unit)
    :RecyclerView.Adapter<LibroAdapter.LibroAdapterViewHolder>(){


    inner class LibroAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        private val binding: ItemLibroBinding = ItemLibroBinding.bind(itemView)

        fun bind(libro: Libros){
            binding.tvNombre.text = libro.nombre
            binding.tvAutor.text = libro.autor
            binding.tvTipo.text = libro.tipo




            binding.root.setOnClickListener {

                itemCallLibro(libro)
            }
        }
    }

    fun updateList(libros: MutableList<Libros>){
        this.libros = libros

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibroAdapter.LibroAdapterViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.item_libro, parent, false)
        return LibroAdapterViewHolder(view)
    }

    override fun getItemCount(): Int {
        return  libros.size
    }

    override fun onBindViewHolder(holder: LibroAdapter.LibroAdapterViewHolder, position: Int) {
        val libro : Libros = libros[position]
        holder.bind(libro)
    }

}